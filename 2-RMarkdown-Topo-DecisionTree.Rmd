---
title: ' Allianz - Memo Technique algorithmes de Machine Learning '
author: "Thibault Jullien"
date: "25/08/2018"
output: "C:/Users/Thibault/Desktop/R Markdown - Decision Tree.html"
html_document: default
---

```{r setup, include=FALSE, message=FALSE, warning=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r message=FALSE, warning=FALSE, include=FALSE}
library(ggplot2)
library(directlabels) 
library(readxl)
library(ggrepel)
library(dplyr)
library(magrittr)
library(ggmap)
library(sp)
library(raster)
library(maptools)
library(rgdal)
library(readxl)
library(spdep)
library(RColorBrewer)
library(stargazer)
library(texPreview)
library(FactoMineR)
library(tidyverse)
library(data.table)
install.packages("C50")
library(C50)
install.packages("classInt")
library(classInt)
install.packages("corrplot")
library(corrplot)
install.packages("knitr")
library(knitr)
library(data.table)
```

#### INTRODUCTION

La r�alisation d'�tudes et d'analyses de Data Science passe par le recours aux algorithmes de machine learning, dont on peut identifier trois �l�ments : 

1) KNN

2) K Means

3) Decision Trees


#### Les algorithmes fond�s sur les Decision Trees

C'est un algorithme supervis� : il a des vis�es pr�dictives et repose sur le partage de l'�chantillon des individus �tudi�s, en deux groupes, avec un �chantillon d'apprentissage et un autre de validation 


Les caract�ristiques des DECISIONS TREES : 
 
- DECISION TREE si variable d�pendante (la variable pr�dite) est qualitative
- d�crire les individus suivant une s�rie de modalit�s
- attribuer un label selon sa position dans l'arbre (le  "feuille") o� elle se trouve
- simple et facile � comprendre mais risque d'over-fitting (correspond si bien aux donn�es d'apprentissage que le mod�le ne peut plus en d�vier pour correspondre aux donn�es out-of-sample)

- les RANDOM FOREST exploitent les DECISION TREE pour une analyse plus sophistiqu�e (chaque nouvel individu est rattach� � un groupe d'apr�s vote majoritaire entre tous les arbres) ce qui permet de r�duire l'overfitting.

On s'int�resse � la pr�diction de l'attribut A : comment r�partir les individus X pour d�terminer au mieux la valeur de A ? Si A est une variable qualitative sur le temps par semaine devant un jeu vid�o, est-ce que l'�ge ou le temps libre par semaine doivent faire partie des caract�ristiques les plus discriminantes? 

D'it�ration en it�ration, l'algorithme choisit une des caract�ristiques C des individus X, capable de pr�dire intelligemment la valeur de A, en commen�ant par C1. Les groupes C11,.....,C1n ainsi identifi�s donnent les premi�res branches de l'arbre. L'agorithme reprend, et s'arr�te quand toutes les caract�ristiques "pr�dictives" sont utilis�es ou quand, pour chaque branche, les attributs A ainsi pr�dits sont tous de la m�me classe (autrement dit,  les groupes d'attributs sont homog�nes)


#### Exemple typique d'algorithme de decision tree : C 50

## Importation et pr�paration des donn�ees

```{r echo=FALSE, include=FALSE, message=FALSE, warning=FALSE}

install.packages("C50")
library(C50)

# Importation, pr�paration et pr�traitement des donn�es

credit <- read.csv("C:/Users/Thibault/Desktop/credit.csv")
str(credit)

# Statistiques descriptives sur deux attributs des individus qui sont susceptibles d'�tre de bons  pr�dicteurs pour la possibilit� de faire d�faut

table(credit$checking_balance)
table(credit$savings_balance)

# On peut v�rifier que certains attributs sont num�riques et ne sont pas susceptibles d'�tre des caract�ristiques pr�dites/� pr�dire par notre algorithme

summary(credit$months_loan_duration)
summary(credit$amount)

# PEUT ETRE STANDARDISER LES VARIABLES QUANTITATIVES
# OU ALORS UNIQUEMENT POUR L'APPRENTISSAGE SUPERVISE ET LES CALCULS DE DISTANCE

# Regarder la r�partition des valeurs (entre yes et no) pour notre variable d'int�r�t, qui r�pond � la question ; les entreprises ont-elles �t� en mesure de rembourser leur pr�t ?

table(credit$default)

```

## S�paration des donn�es entre �chantillon d'apprentissage et �chantillon de validation

# On se pr�pare � utiliser la fonction order. En l'occurence, on va ranger les valeurs de la variable credit dans un ordre al�atoire, en utilisant la fonction runif(1000). On utilise ainsi la syntaxe credit[order(1000)),]. 

```{r }

# Pr�paration de l'algorithme : cr�er des jeux de donn�es al�atoires qui puissent servir d'�chantillon d'apprentissage et d'�chantillon de validation 

set.seed(12345)
credit_rand <- credit[order(runif(1000)), ]

summary(credit$amount)
summary(credit_rand$amount)

head(credit$amount)
head(credit_rand$amount)

```

# On peut v�rifier que la variable "amount" de la table credit et la variable "amount" de credit_rand sont bien les deux m�mes variables dans des ordres diff�rents en affichant les statistiques descriptives avec summary. Cependant, en affichant les quelques premi�res valeurs des variables avec head, on voit qu'elles sont bien rang�es diff�remment.

```` {r }

# On peut ensuite proc�der � la s�paration de l'�chantillon des individus �tudi�s entre un �chantillon d'apprentissage et un �chantillon de validation.

credit_train <- credit_rand[1:900, ]
credit_test <- credit_rand[901:1000, ]

# Piste d'am�lioration : faire cette r�partition �chantillon apprentissage / sample de validation avec un tirage al�atoire ou n'importe quelle fonction de R pr�vue pour que les proportions de chaque classe/caract�ristique soient les m�mes dans chaque �chantillon.


# On v�rifie que la proportion d'entreprises en d�faut / d'entreprises qui ne sont pas en d�faut est la m�me dans les deux �chantillons

prop.table(table(credit_train$default))
prop.table(table(credit_test$default))

```

## Cr�ation de l'arbre de d�cision 

```{r echo=FALSE, include=FALSE, message=FALSE, warning=FALSE}

# ****************** Cr�ation de l'arbre de d�cision **********************************

factor_default <- credit_train$default

credit_train$default<-as.factor(credit_train$default)
  
credit_model <- C5.0(credit_train[-21], credit_train$default)

# On utilise la fonction c5.0 du package "C50" qui prend en arguments : a)l'�chantillon d'apprentissage, b) le type de classe de chaque ligne de l'�chantillon d'apprentissage c) �ventuellement,"trials=k" pour le nombre d'it�rations d) �ventuellement, costs=  pour indiquer une matrice sp�cifiant les co�ts associ�s avec certains types d'erreurs

credit_model

# permet d'avoir des informations basiques sur l'arbre du type : le nombre de features(attributs) c'est-�-dire le nombre de pr�dicteurs, ainsi que le nombre d'exemples/d'�chantillons utilis�s pour faire croitre l'arbre

summary(credit_model)

# Ici les stats desc. de summary permettront surtout de faire apparaitre les d�cisions de l'arbre de d�cision 


#  Par exemple, avec notre fichier, nous verrions se d�rouler la logique de Decision Tree suivante, bas�e sur l'analyse d'attributs, le choix des attributs les plus pertinents et l'exclusion des autres pour pr�dire de mieux en mieux la proba de d�faut ou la classe "d�faut"/"pas de d�faut" : 

# 1) quand le courant est inconnu, classifier l'individy dans la classe "d�faut" de notre variable d'int�r�t   2) sinon, si le compte courant est inf�rieur � un seuil A, alors... si ce compte couurant est compris entre un seuil A et un seuil B, alors...    3) si l'historique des cr�dits est tr�s bon, alors... 

```



## Pr�dictions, arbre de d�cision et �valuation de la qualit� du mod�le

```{r echo=FALSE, include=FALSE, message=FALSE, warning=FALSE}

# ***************   FACE AU RISQUE D'OVERFITTING, EVALUER LA QUALITE DU MODELE *****************

credit_pred <- predict(credit_model, credit_test)

# On utilise la fonction predict qui permet de r�aliser des pr�dictions et prend en arguments a) un mod�le appris sur le mod�le c5.0 b) un �chantillon de validation c)type= qui est soit "type=class" soit "type=prob" qui indique si la pr�diction nous donnera la classe d'objet la plus probable (option par d�faut) ou des probabilit�s brutes pour chaque classe d'objet

# credit_pred est un vecteur de diff�rentes classes pr�dites via l'algo

# on peut alors comparer cette s�rie de pr�dictions avec les valeur de classes effectives gr�ce � la fonction CrossTable. On choisit ici de montrer les pourcentages relatifs aux colonnes et aux lignes (optionss TRUE pour prop.c et prop.r). On a avec prop.t la proportion d'observations dans la cellule pa rapport au nombre total d'observations.

install.packages("gmodels")
library(gmodels)

CrossTable(credit_test$default, credit_pred, prop.chisq = FALSE, prop.c = FALSE, prop.r = FALSE, dnn = c('actual default', 'predicted default'))

```

## Am�lioration des performances du mod�le

On va voir ici une tentative d'am�lioration en augmentant le boosting de l'arbre. Attention, des m�thodes de genre type Gradient Boosting Tree ne sont pas identiques au principe des Random Forest.

```{r echo=FALSE, include=FALSE, message=FALSE, warning=FALSE}

# ************* L'AMELIORATION DES PERFORMANCES DU MODELE **********************

credit_boost10 <- C5.0(credit_train[-17], credit_train$default, trials = 10)

# On utilise ici une r�f�rence dans le monde des professionnels et de la recherche : fixer � 10 le nombre d'arbres maximum (le nombre d'it�rations des tentatives de construire un arbre) � construire pour pr�dire la classe de la variable d'int�r�t, sachant que l'algo arr�te les it�rations de toute fa�on s'il per�oit qu'une it�ration suppl�mentaire est impuissante � augmenter le pouvoir de pr�diction du mod�le.

credit_boost10

# On v�rifie qu'apr�s un certain nombre d'it�rations la taille des arbres se r�duit.

summary(credit_boost10)

#fait apparaitre un tableau � 4 valeurs : �tre pr�dit en Yes et avoir la valeur Yes, �tre pr�dit en Yes et avoir la valeur No... Normalement, on voit que le nombre d'erreurs de pr�dictions est faible

# L'am�lioration du mod�le est donc sensible 

# N�cessaire de v�rifier la robustesse du r�sultat en v�rifiant si l'am�lioration des r�sultats se confirme sur l'�chantillon de validation : 


credit_boost_pred10 <- predict(credit_boost10, credit_test)

CrossTable(credit_test$default, credit_boost_pred10, prop.chisq = FALSE, prop.c = FALSE, prop.r = FALSE, dnn = c('actual default', 'predicted default'))

# NB pour supprimer un objet : rm(credit_model)

```
